## Session 1 - Vinetan Weapons

![vineta](../world_files/images/vineta.png)

### Players
* Kelly as Dirk Diggler, a pugalist wrestler
* Lena as Gonte the goblin who Gobbles deez nuts
* Allan as The Empire builder, Empire Builder is his son.
* Nick as Xerxes the Kencu Raven, master of memory and mimicry
* Jackson as Bruce, the magical simp.

### Mission Overview

#### The assignment
The adventure began with the team being dispatched to Vineta, a city known for its magical weapons production. Their mission was to disrupt this industry following the recent overthrow of King Alaric by the Haldorson brothers. Alaric's daughter, Airliss, had fled into the nearby forest, setting the stage for potential alliances.

#### Arrival and Initial Conflict
Upon arriving at Vineta's port, the team narrowly avoided trouble when Gonte defaced a wall. Luckily, the guards, mistaking Dirk for a professional wrestler, were distracted and headed to the 'Round-Ears' bar, allowing the group to follow and gather information. Inside the bar, Dirk gained favor with local soldiers while Bruce encountered a mysterious, disgruntled figure who quickly left, sparking Bruce's curiosity and subsequent stealthy pursuit with the team.

#### Escalation and Combat
Their actions raised suspicions, leading to a confrontation with the guards. When questioned, Xerxes struck the guard leader, igniting a fierce battle. The leader, enhanced by a magical sword, grew in size but was ultimately defeated by the team, who then fled with the sword, escaping to the forest for a brief respite.

#### Alliance with Airliss
In the forest, the team encountered Airliss beside a giant brown bear in a clearing. After a cautious dialogue, Airliss agreed to guide them to the weapons factory in exchange for their help in eliminating the Haldorson brothers. Trust established, they spent the night in the woods, planning their next move.

#### Infiltrating the Factory
The following day, fortified by coffee and equipped with insights from a local mouse informant, the team reentered the city. Utilizing an air duct discovered by Gonte, Xerxes scouted the factory, spotting various adversaries including a mage, an apprentice, and an array of skeletons and chickens.

#### Decisive Battle
Launching their attack, Airliss used her magic to halt the factory's operations via an "EMERGENCY STOP" button controlled by a chicken. Chaos ensued as the stop mechanism temporarily froze everyone except Gonte and Xerxes, who dispatched several skeletons. The mage revealed as Gaius from the bar, shared crucial information before his demise.

#### Climactic Confrontation
The freed druid, Thornvir, transformed into a triceratops using enlargement weapons and rampaged towards the capital. Airliss, determined to protect her city, pursued him, with the team following. In the capital, they faced both Thornvir and the Haldurson brothers in a complex battle, ultimately triumphing but at great cost.

#### Fin
In the aftermath, Bruce opted to remain in Vineta, joining Airliss's guard. The rest of the team, reflecting on their journey, enjoyed a serene sunset on the pier before sailing back to Camelot and Dark Gate Prison, marking the end of a tumultuous but victorious mission.

![the rampage](../world_files/images/imprisoned_thornvir.png)