
### Ability reference
```
Shield : reaction : lvl 1 spell : : until next turn
+5 to AC

Grease : action : lvl 1 spell : 5ft radius within 60ft : 10 turns
Makes affected ground difficult terrain. starting or enterint Grease forces a dex save that inflicts prone on fail

Enlarge : action : lvl 2 spell : 30 ft : 10 turns
A target increases in size, gains str+A and +1d4 weapon damage

Enrage : Bonus Action : : : 10 turns
Enter a rage - take half damage from physical sources, str+A, gain +prof damage

Compelled Duel : bonus action : lvl 1 spell : 30 ft : up to 1 min
Compels a creature to duel you, disadvantage on attacks against others, must make a Wisdom save to move more than 30 ft away from you.

Divine Favor : bonus action : lvl 1 spell : self : up to 1 min
Your weapon attacks deal an extra 1d4 radiant damage on a hit.

Glyph of Warding : action : lvl 3 spell : 30 ft : until triggered or dispelled
Creates a glyph that stores a spell to be triggered under specific circumstances.

Flesh to Stone : action : lvl 6 spell : 60 ft : up to 1 min
Attempt to turn one creature into stone. Creature must make repeated Constitution saves or be petrified. First to 3 saves or 3 fails. One save on cast, then one save on each turn.

Thunderwave : action : lvl 1 spell : self (15-ft cube) : 
Deals 2d8 thunder damage and pushes away creatures in range; con save for half damage and no push.

Scorching Ray : action : lvl 2 spell : 120 ft : instantaneous
Create three rays of fire and hurl them at targets within range. Each ray hits with a ranged spell attack for 2d6 fire damage.

Burning Hands : action : lvl 1 spell : self (15-ft cone) : instantaneous
Shoots fire from your fingertips, dealing 3d6 fire damage to each creature in area; dex save for half damage.
```