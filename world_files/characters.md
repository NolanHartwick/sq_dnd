### Camelot
```
Amanda Fencer
Dak Gate Prison Warden
Alive
A powerful and mysterious figure whose puppets operate both inside and outside the prison
```

### Vineta
```
Alaric
Former King of Vineta
Deceased
Led Vineta through a period of prosperity and peace but ultimately fell to his cities nativism

Airliss
Princess of Vineta
Alive
Chased off into the forest surrounding Vineta. Known to have a strong affinity for wildlife

Knut Haldorson
Defacto ruler of Vineta
Deceased
A warrior with some knowledge of spell casting, the oldest of the Haldorson brothers

Bjorn Haldorson
Defacto Marshal of Vineta
Deceased
A beserker known for brutalizing anyone who annoys him into anger

Sten Haldorson
Defacto Marshal of Vineta
Deceased
A skilled swordsman who takes pride in their fencing abilities.

Gaius
Head Wizard for Haldorsons
Deceased
A crotchety old elven wizard, coerced to some degree into working for the Haldurson's

Lyra
Apprentice to Gaius
Deceased
A young elven wizard trying to survive in a tough city

Thornvir
Alive
An imprisoned drow druid, enslaved by the haldurson brothers
```
