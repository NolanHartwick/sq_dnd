1. Camelot - The legendary castle and court associated with King Arthur. In our tale, it is the home of our heros and villians. Amanda Fencer runs Dark Gate prison, the highest security and most brutal prison in the land.
2. Vineta - In real life, a mythical city said to have been on the southern coast of the Baltic Sea and sunk into the sea due to the sins of its inhabitants. In our world, Led by former king Alaric, survived by his daughter Airliss, now controlled by the Haldorson brothers: Knut, Bjorn, and Sten. They, along with Gaius and Lyra are vastly upscaling their enchanted weapons productions in some poorly understood power play.


### Unusued Legends
```
Atlantis - The well-known island said to have sunk into the Atlantic Ocean, according to Plato's dialogues.
El Dorado - The legendary city of gold, sought after by many explorers in South America.
Shangri-La - A fictional paradise, often depicted as a Himalayan utopia, isolated from the world.
Avalon - A mystical island featured in Arthurian legend, where Excalibur was forged and King Arthur was taken to heal after his final battle.
Agartha - A mythical city that is said to reside in the Earth's core.
Hyperborea - A mythical land in Greek mythology, located far to the north and home to a race of giants.
Lemuria - A supposed lost land located either in the Indian or Pacific Ocean.
Lyonesse - A legendary lost land said to have sunk into the sea, located off the coast of Cornwall.
Ys - A mythical city said to have been on the coast of Brittany, France, that was submerged under the ocean.
Zerzura - A white city oasis described in Arabian folklore that is said to exist in the Sahara desert.
Asphodel Meadows - In Greek mythology, a section of the underworld where ordinary souls were sent to live after death.
Cibola - One of the legendary Seven Cities of Gold that Spanish explorers believed existed in the New World.
Quivira - A mythical city of wealth, sought by Francisco Vásquez de Coronado in North America.
Biringan City - A legendary invisible city said to appear at night in the Philippines, home to supernatural beings.
Cantre'r Gwaelod - A legendary sunken kingdom in Welsh mythology, sometimes referred to as the "Welsh Atlantis."
Kitezh - A mythical city in Russian folklore that was said to have sunk beneath Lake Svetloyar to protect itself from Mongol invaders.
Tír na nÓg - The “Land of the Young” in Irish mythology, an otherworldly realm of everlasting youth, beauty, health, and joy.
Shambhala - A mythical kingdom in Buddhist tradition, said to be hidden somewhere in Inner Asia, it is associated with the idea of a paradise.
```