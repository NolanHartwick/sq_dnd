![Cabinet](./cabinet.png)

<table>
    <tr>
        <td><img src="./items/lazarus_blade.png" alt="Lazarus Blade"></td>
        <td>
            <h2>Lazarus Blade</h2>
            <p>Versatile Sword - 1d8 + DEX/STR modifier slash damage</p>
            <p>Reaction : Once per day : When you are reduced to zero hit points, recover back to full health instead.</p>
            <p><em>Forged in a hidden Lazarus pit, this blade carries with it the curse of eternal life.</em></p>
        </td>
    </tr>
    <tr>
        <td><img src="./items/thunderfist.png" alt="Thunderfist"></td>
        <td>
            <h2>Thunderfist</h2>
            <p>One-handed Mace - 1d8 + STR bludgeon damage</p>
            <p>Action : Once per combat : Deal LEVELd8 lightning damage to a target within 30 ft.</p>
            <p><em>Imbued with the storm's fury and debauchery, Thunderfist crackles with the relentless energy of the tempest.</em></p>
        </td>
    </tr>
    <tr>
        <td><img src="./items/vibranium_warhammer.png" alt="Vibranium Warhammer"></td>
        <td>
            <h2>Vibranium Warhammer</h2>
            <p>One-handed hammer - 1d8 + STR/DEX bludgeon damage</p>
            <p>Reaction : Once per combat : When an attack misses you, your next successful attack deals an additional LEVELd8 thunder damage.</p>
            <p><em>Stolen long ago from a far away land. It can absorb almost any blow and turn it back on your opponents.</em></p>
        </td>
    </tr>
    <tr>
        <td><img src="./items/kill_shot.png" alt="The Kill Shot"></td>
        <td>
            <h2>The Kill Shot</h2>
            <p>Short Bow - 1d8 + DEX pierce damage</p>
            <p>Reaction : Once per combat : When you attack successfully, upgrade it to a critical hit.</p>
            <p><em>With a whisper, The Kill Shot releases death; as inevitable as fate, as precise as time.</em></p>
        </td>
    </tr>
    <tr>
        <td><img src="./items/collosus_skin.png" alt="Bronze Skin"></td>
        <td>
            <h2>Brozne Skin</h2>
            <p>Reaction : Once per combat : When something would damage you, it doesn't.</p>
            <p><em>Like the legendary titans of old, this skin turns away steel and spell alike, leaving the wearer unscathed.</em></p>
        </td>
    </tr>
    <tr>
        <td><img src="./items/angels_wings.png" alt="Angel's Wings"></td>
        <td>
            <h2>Angel's Wings</h2>
            <p>Bonus Action : Once per combat : You can fly for the next 10 turns. You can carry up to one ally. (movement is doubled while flying)</p>
            <p><em>Gifted from the heavens, Angel's Wings carry heroes above the fray, where hope soars and falls.</em></p>
        </td>
    </tr>
    <tr>
        <td><img src="./items/flame_on.png" alt="Flame On"></td>
        <td>
            <h2>Flame On</h2>
            <p>Bonus Action : Once per day : Deal 1d6 fire damage to all enemies within 5ft at the end of your turn for the next 10 turns.</p>
            <p><em>Ignited by a hero's fiery heart, Flame On burns with an unquenchable blaze that devours all in its path.</em></p>
        </td>
    </tr>
    <tr>
        <td><img src="./items/psyblade_infusion.png" alt="Psyblade Infusion"></td>
        <td>
            <h2>Psyblade Infusion</h2>
            <p>Bonus Action : Once per day : Your attacks deal an additional 1d8 psychic damage for 10 turns</p>
            <p><em>Wielded by the mind alone, Psyblade slices through the corporeal to strike directly at the soul.</em></p>
        </td>
    </tr>
    <tr>
        <td><img src="./items/fries_frosty.png" alt="Fries Frosty"></td>
        <td>
            <h2>Fries Frosty</h2>
            <p>Action : Once per combat : Deal LEVELd8 cold damage to any target within 30 ft.</p>
            <p><em>When this icy spell hits, enemies will find their plans not just foiled, but frozen. It's snow laughing matter.</em></p>
        </td>
    </tr>
    <tr>
        <td><img src="./items/reds_reality_journal.png" alt="Red's Reality Journal"></td>
        <td>
            <h2>Red's Reality Journal</h2>
            <p>Reaction : Once per day : Whenever someone does a save or check, you may make them succeed or fail.</p>
            <p><em>Inscribed with destinies averted, the secrets to bending fate live within.</em></p>
        </td>
    </tr>
    <tr>
        <td><img src="./items/quiet_kings_crown.png" alt="Quiet King's Crown"></td>
        <td>
            <h2>Quiet King's Crown</h2>
            <p>Action : Once per day : Deal LEVELd8 thunder damage to everything else within 30 ft.</p>
            <p><em>Sticks and stones may break your bones, but words will disentrigrate.</em></p>
        </td>
    </tr>
    <tr>
        <td><img src="./items/doctors_hood.png" alt="Doctor's Hood"></td>
        <td>
            <h2>Doctor's Hood</h2>
            <p>Action and Bonus Action : Once per day : Create a copy of yourself for three turns. It goes next. When it takes damage, it goes away.</p>
            <p><em>Woven from the threads of deception and shadows, the Doctor's Hood conjures an illusion so lifelike, it fools even the keenest eye, a spectral duplicate dancing to the puppeteer's tune.</em></p>
        </td>
    </tr>
    <tr>
        <td><img src="./items/first_domino.png" alt="The First Domino"></td>
        <td>
            <h2>Dice and Domino</h2>
            <p>Add 1d4 to all attack rolls, checks, and saves. (not damage rolls)</p>
            <p><em>The last weilder managed to be struck by lightning at the same time a sinkhole swalloed them. A coroner later confirmed that a simultaneous aneurysm was the actual cause of Death.</em></p>
        </td>
    </tr>
    <tr>
        <td><img src="./items/utility_box.png" alt="The Utility Box"></td>
        <td>
            <h2>The Utility Box</h2>
            <p>Bonus Action : Once per combat : Deal LEVELd6 damage of any type to any target within 30 ft. (or appropriate gadgety effect)</p>
            <p><em>From the depths of ingenuity and a wealthy man's bank account springs The Utility Box, a marvel of adaptability in the throes of chaos.</em></p>
        </td>
    </tr>
    <tr>
        <td><img src="./items/twine_of_truth.png" alt="Twine of Truth"></td>
        <td>
            <h2>Twine of Truth</h2>
            <p>Action : Once per day : A target within 30 ft. has no movement and must answer questions honestly until your next turn.</p>
            <p><em>The Twine of Truth, unbreakable and unyielding, binds the tongue to the heart, revealing secrets dark and deep.</em></p>
        </td>
    </tr>
</table>