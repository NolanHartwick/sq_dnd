import argparse
import cv2

def split_image(image_path):
    # Load the image using OpenCV
    img = cv2.imread(image_path)

    # Check if the image was loaded successfully
    if img is None:
        raise ValueError("Image could not be loaded. Please check the file path.")

    # Calculate the split index
    split_index = img.shape[0] // 2

    # Split the image
    top_half = img[:split_index, :]
    bottom_half = img[split_index:, :]

    return top_half, bottom_half

def save_images(top_half, bottom_half, prefix, image_type):
    # Save the top half
    cv2.imwrite(f"{prefix}.top.{image_type}", top_half)

    # Save the bottom half
    cv2.imwrite(f"{prefix}.bottom.{image_type}", bottom_half)

def main():
    # Set up argument parser
    parser = argparse.ArgumentParser(description="Split an image into top and bottom halves.")
    parser.add_argument("image_path", type=str, help="Path to the input image.")

    # Parse arguments
    args = parser.parse_args()

    # Extract prefix and image type from the file name
    parts = args.image_path.split('.')
    if len(parts) < 2:
        raise ValueError("Invalid image filename. Please include an extension.")

    prefix = '.'.join(parts[:-1])
    image_type = parts[-1]

    # Process the image
    top_half, bottom_half = split_image(args.image_path)

    # Save the output images
    save_images(top_half, bottom_half, prefix, image_type)

if __name__ == "__main__":
    main()

